public class GameManager{
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	public GameManager() {
		this.drawPile = new Deck();
		drawPile.shuffle();
		this.centerCard = drawPile.drawTopCard();
		this.playerCard = drawPile.drawTopCard();
	}
	
	public String toString() {
		return "Center card: " + this.centerCard + "\n" + "Player card: " + this.playerCard;
	}
	 public void dealCards() {
		Card firstCard = drawPile.drawTopCard();
		playerCard = firstCard;
		this.drawPile.shuffle();
		Card secondCard = drawPile.drawTopCard();
		centerCard = secondCard;
		this.drawPile.shuffle();
		 
	 }
	 
	 public int getNumOfCards() {
		 int NumOfCards = drawPile.length();
		 return NumOfCards;
	 }
	 
	 public int calculatePoints() {		
		if (this.playerCard.getSuit().equals(this.centerCard.getSuit())){
				return 2;
			}
		if (this.playerCard.getValue().equals(this.centerCard.getValue())){
			return 4;
			}
		else{
			return -1;
			}
	 }
}