import java.util.Scanner;

public class LuckyCardGameApp{
	public static void main(String[] args){
		GameManager manager = new GameManager();
		int totalPoints = 0;
		System.out.println("Welcome to the Lucky Card Game!");
		
		while(manager.getNumOfCards() > 2 && totalPoints < 5){
			System.out.println(manager);
			totalPoints = totalPoints + manager.calculatePoints();
			System.out.println("Your total points for this round is: " + totalPoints + " points" + "\n");
			manager.dealCards();
		}
		
		if(totalPoints > 5){
			System.out.println("Congratulations! You won the game with " + totalPoints + " points");
		}
		else{
			System.out.println("Unfortunately, you lost the game with " + totalPoints + " points");
		}
		
	}
}